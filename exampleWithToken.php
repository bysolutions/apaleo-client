<?php
/**
 * Simple example for connecting to the Apaleo API and retreiving the available inventory.
 *
 * Copyright 2017 Loopon AB. See LICENSE for details (MIT License; free to use as you wish).
 *
 * Using OAuth2 library provided at:
 *     https://github.com/thephpleague/oauth2-client
 *
 * In order to download/install required libraries, first install composer:
 *     https://getcomposer.org/doc/00-intro.md
 *
 * Then download the required dependencies:
 *     $ composer install
 *
 * Finally update the credentials at the bottom of this file:
 *     $lApaleoExample = new ApaleoClient('<URL OF YOUR APPLICATION>', '<USERNAME>', '<PASSWORD>');
 *
 * This code is provided as-is without warranty of any kind. If you do have questions/need assistance with this example,
 * feel free to contact Simon Finne <simon.finne@loopon.com> where I will try to respond if possible, but cannot
 * guarantee any support.
 *
 * Note that functionality of this exact example depends on your user having the properties.read scope.
 * Also note, that if you for any reason want to connect to Apaleo's staging environment, you need to update the
 * ApaleoProvider::cIdentityUrl and ApaleoClient::cApiUrl configurations.
 *
 * Contact api@apaleo.com or see dev.apaleo.com for help regarding their api.
 */

require(__DIR__ . '/vendor/autoload.php');
require(__DIR__ . '/src/Client.php');

$token = '';
$refresh_token = '';
$expireDate = '';

$appClientId = '';
$appClientSecret = '';

$propertyId = '';

$dt = DateTime::createFromFormat('Y-m-d H:i:s', $expireDate);

$options = [
    'access_token' => $token,
    'refresh_token' => $refresh_token,
    'expires' => $dt->getTimestamp()
];


$accessToken = new \League\OAuth2\Client\Token\AccessToken($options);

$client = new \BYSolutions\Apaleo\Client\Client();
$client->initWithUserCredentials($accessToken, $appClientId, $appClientSecret);

$filterFrom = \DateTime::createFromFormat(DateTime::ATOM, '2019-04-19T00:00:00+00:00');
$filterTo = \DateTime::createFromFormat(DateTime::ATOM, '2019-05-31T00:00:00+00:00');



//$res = $client->getRatePlanApi()->getRatePlansRates('ZJUAM-BAR_BB_DBL', $filterFrom->format(DateTime::ATOM), $filterTo->format(DateTime::ATOM));

//echo "<pre>";
//var_dump($res);
//echo "</pre>";
//exit;

$filterFrom = \DateTime::createFromFormat(DateTime::ATOM, '2019-06-30T00:00:00+00:00');
$filterTo = \DateTime::createFromFormat(DateTime::ATOM, '2020-06-30T23:59:59+00:00');


$params = [
    'propertyIds' => [$propertyId],
    'from' => $filterFrom->format(DateTime::ATOM),
    'to' => $filterTo->format(DateTime::ATOM),
    'pageNumber' => '1',
    'pageSize' => '100'

];


$blocks = $client->getBookingApi()->getBlocks($params);
echo "<pre>";
var_dump($blocks);
echo "</pre>";
exit;

$reservations = $client->getBookingApi()->getReservationsBy([
    'propertyIds' => [$propertyId],
    'from' => $filterFrom->format(DateTime::ATOM),
    'to' => $filterTo->format(DateTime::ATOM),
    'dateFilter' => 'Stay',
    'pageNumber' => 1,
    //'pageSize' => 10000,
    'expand' => ['timeSlices', 'services'],

]);
/*$rns = 0;
foreach($reservations as $r) {
    foreach($r->timeSlices as $ts) {
        if(substr($ts->from, 0, 10) == '2019-06-30') {
            $rns ++;
        }
    }
}
echo $rns;exit;*/



echo "<pre>" . json_encode($reservations, JSON_PRETTY_PRINT) . '</pre>';
exit;


