# ApaleoClient

Simple PHP Library for interacting with the Apaleo Open Hospitality Cloud (www.apaleo.com)

## Getting Started

This code has been written for and tested in PHP 7.1. It will not work on anything earlier than PHP 7 due to the usage of strict typing.

To do anything meaningful with this code, you first need to obtain user credentials from Apaleo; contact them at api@apaleo.com for details.

### Prerequisites

In order to use this example client, you need to install composer. See https://getcomposer.org/doc/00-intro.md

### Creating a completely new project using the ApaleoClient

Once you have composer installed, create an empty new project and create a `composer.json` file with the following contents:



```
$ composer bysolutions/apaleo-client
```

Now create a new file `ExampleClient.php` with the following contents:

```
<?php
require(__DIR__ . '/vendor/autoload.php');

$client = new BYSolutions\Apaleo\Client\Client();
$client->initWithUnitCredentials('<URL OF YOUR APPLICATION>', '<USERNAME>', '<PASSWORD>');
$response = $client->getInventoryApi()->getProperties();
print_r($response);
```



## Testing the client

Simply run the code:

```
$ php ExampleClient.php
```

If you have setup your credentials correctly, the example will simply print the available inventory for your user. In case you only see an error message or stack trace, you most likely have provided incorrect credentials. 


## Built With

* [The PHP League OAuth2](https://github.com/thephpleague/oauth2-client) - OAuth 2.0 Client


## Authors

* **Simon Finne** - *Initial implementation* - [simonfi](https://github.com/simonfi)
* **BYSolutions **


## License

This project is licensed under the 3-Clause BSD License - see the [LICENSE](LICENSE) file for details


## Acknowledgments

* Thank you to Andrea Stubbe at Apaleo for all the help while building Loopon's integration to the Apaleo Open Hospitality Cloud


