<?php
/**
 * PHP Implementation of Apaleo API.
 *
 * Copyright 2017 Loopon AB. See LICENSE for details (MIT License; free to use as you wish).
 *
 * This code is provided as-is without warranty of any kind. If you do have questions/need assistance with this example,
 * feel free to contact Simon Finne <simon.finne@loopon.com> where I will try to respond if possible, but cannot
 * guarantee any support.
 *
 * Note, that if you for any reason want to connect to Apaleo's staging environment, you need to update the
 * ApaleoProvider::cIdentityUrl and ApaleoClient::cApiUrl configurations.
 *
 * Contact api@apaleo.com or see dev.apaleo.com for help regarding their api.
 */

namespace BYSolutions\Apaleo\Client;

/**
 * @brief Implementation of Apaleo RatePlan API
 */
class RatePlanApi extends ApiBase
{
    /**
     * @brief Get and return the available rate plans for the given property
     */
    public function getRatePlans(string $pPropertyId,
                                 int $pPageNumber = 1,
                                 int $pPageSize = 100)
    {
        $lParametersArray = [];


        try{
            $lRatePlans = $this->get('rateplan/v1/rate-plans',
                [
                    'propertyId' => $pPropertyId,
                    'pageNumber' => '' . $pPageNumber,
                    'pageSize' => '' . $pPageSize
                ]);
        } catch (\Exception $e) {
            return [];
        }


        if (!isset($lRatePlans->ratePlans)) {
            return [];
        }

        return $lRatePlans->ratePlans;
    }

    public function getRatePlansRates($ratePlanId, $from, $to, int $page = 1,
                                      int $size = 100)
    {

        $result = $this->get('rateplan/v1/rate-plans/' . $ratePlanId . '/rates',
            [
                'from' => $from,
                'to' => $to,
                'pageNumber' => '' . $page,
                'pageSize' => '' . $size
            ]);



        if (!isset($result->rates)) {
            return [];
        }

        return $result->rates;
    }

    /**
     * @brief Get and return information (in all languages) of the given rate plane
     */
    public function getRatePlan(string $pRatePlanId)
    {
        $lProperty = $this->get('rateplan/v1/rate-plans/' . $pRatePlanId,
            [
                'languages' => 'ALL',
                'expand' => 'cancellationPolicy'
            ]);

        return $lProperty;
    }

    public function getServices(string $pPropertyId,
                                int $pPageNumber = 1,
                                int $pPageSize = 100)
    {
        $lServices = $this->get('rateplan/v1/services', [
            'propertyId' => $pPropertyId,
            'pageNumber' => '' . $pPageNumber,
            'pageSize' => '' . $pPageSize
        ]);
        return $lServices;

    }

    public function getCompany(string $companyId)
    {
        return $this->get('rateplan/v1/companies/'.$companyId, []);
    }

    public function getCompanies(string $propertyId, int $pageNumber = 1, int $pageSize = 100)
    {
        try{
            $data = $this->get('rateplan/v1/companies', [
                'propertyId' => $propertyId,
                'pageNumber' => '' . $pageNumber,
                'pageSize' => '' . $pageSize
            ]);
        } catch (\Exception $e) {
            return [];
        }

        if(isset($data->companies)) {
            return $data->companies;
        }
        return [];
    }
}
