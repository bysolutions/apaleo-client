<?php

namespace BYSolutions\Apaleo\Client;

/**
 * @brief Authentication provider for the Apaleo basic authentication scheme
 */
class Provider extends \League\OAuth2\Client\Provider\GenericProvider
{
    /**
     * @var string
     */
    private $mClientId;

    /**
     * @var string
     */
    private $mClientSecret;

    const cIdentityUrl = 'https://identity.apaleo.com/';

    public function __construct(string $pClientId, string $pClientSecret)
    {


        parent::__construct([
            'urlAccessToken' => self::cIdentityUrl . 'connect/token',
            'redirectUri' => '',
            'urlAuthorize' => '',
            'urlResourceOwnerDetails' => '',
            'scopes' => [
                'openid',
                'profile',
                'properties.read',
                'units.read',
                'account.read',
                'reservations.read',
                'rates.read',
                'services.read',
                'offline_access',
                'transactions.export',
            ]
        ]);

        $this->mClientId = $pClientId;
        $this->mClientSecret = $pClientSecret;
    }






    protected function getDefaultHeaders()
    {
        return ['Authorization' => 'Basic ' . base64_encode($this->mClientId . ':' . $this->mClientSecret)];
    }
}