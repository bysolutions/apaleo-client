<?php
/**
 * PHP Implementation of Apaleo API.
 *
 * Copyright 2017 Loopon AB. See LICENSE for details (MIT License; free to use as you wish).
 *
 * This code is provided as-is without warranty of any kind. If you do have questions/need assistance with this example,
 * feel free to contact Simon Finne <simon.finne@loopon.com> where I will try to respond if possible, but cannot
 * guarantee any support.
 *
 * Note, that if you for any reason want to connect to Apaleo's staging environment, you need to update the
 * ApaleoProvider::cIdentityUrl and ApaleoClient::cApiUrl configurations.
 *
 * Contact api@apaleo.com or see dev.apaleo.com for help regarding their api.
 */

namespace BYSolutions\Apaleo\Client;

use League\OAuth2\Client\Token\AccessToken;
use BYSolutions\Apaleo\Client\Provider;


/**
 * @brief Apaleo API client, which can authenticate and use the token to GET information from the API
 */
class Client
{
    /**
     * @brief The referrer url provided when calling the Apaleo api
     * @var string
     */
    private $mClientUrl;

    /**
     * @brief Grant provider for Apaleo API
     */
    private $mApaleoProvider;

    /**
     * @brief Cached authentication token after it has been retrieved once, null up until that point
     * @var AccessToken
     */
    private $mToken = null;

    /**
     * @var AccountApi
     */
    private $mAccountApi = null;

    /**
     * @var BookingApi
     */
    private $mBookingApi = null;

    /**
     * @var InventoryApi
     */
    private $mInventoryApi = null;

    /**
     * @var RatePlanApi
     */
    private $mRatePlanApi = null;

    /**
     * @var FinanceApi|null
     */
    private $mFinanceApi = null;

    private $cApiUrl = self::cApiUrl;

    const cApiUrl = 'https://api.apaleo.com/';

    public function __construct()
    {

    }

    /**
     *
     * Use this to initialize the client with given unit credentials
     *
     * @param $clientUrl
     * @param $clientId
     * @param $clientSecret
     */
    public function initWithUnitCredentials($clientUrl, $clientId, $clientSecret)
    {
        $this->mClientUrl = $clientUrl;
        $this->mApaleoProvider = new Provider($clientId, $clientSecret);
    }

    /**
     * Use this to initalize the client with given user credentials by using application client id and secret
     *
     * @param AccessToken $token
     * @param $appClientId
     * @param $appClientSecret
     */
    public function initWithUserCredentials(AccessToken $token, $appClientId, $appClientSecret)
    {
        $this->mToken = $token;
        $this->mApaleoProvider = new Provider($appClientId, $appClientSecret);
    }

    public function setToken(AccessToken $token)
    {
        $this->mToken = $token;
    }

    /**
     * @brief Return BookingApi, constructing it if needed
     * @return BookingApi
     */
    public function getBookingApi(): BookingApi
    {
        if ($this->mBookingApi === null) {
            $this->mBookingApi = new BookingApi($this);
        }

        return $this->mBookingApi;
    }

    public function getFinanceApi(): FinanceApi
    {
        if($this->mFinanceApi === null) {
            $this->mFinanceApi = new FinanceApi($this);
        }
        return $this->mFinanceApi;
    }

    /**
     * @brief Return AccountApi, constructing it if needed
     * @return AccountApi
     */
    public function getAccountApi(): AccountApi
    {
        if ($this->mAccountApi === null) {
            $this->mAccountApi = new AccountApi($this);
        }

        return $this->mAccountApi;
    }

    /**
     * @brief Return InventoryApi, constructing it if needed
     * @return InventoryApi
     */
    public function getInventoryApi(): InventoryApi
    {
        if ($this->mInventoryApi === null) {
            $this->mInventoryApi = new InventoryApi($this);
        }

        return $this->mInventoryApi;
    }

    /**
     * @brief Return RatePlanApi, constructing it if needed
     * @return RatePlanApi
     */
    public function getRatePlanApi(): RatePlanApi
    {
        if ($this->mRatePlanApi === null) {
            $this->mRatePlanApi = new RatePlanApi($this);
        }

        return $this->mRatePlanApi;
    }

    /**
     * @brief Return the received authentication token, or request and return it if it's the first call
     */
    private function getToken()
    {
        // If we don't already have the access token, request it
        if ($this->mToken === null) {
            $this->mToken = $this->mApaleoProvider->getAccessToken('client_credentials', []);
        }

        // Check and renew token if needed
        $this->checkAndRenewToken();

        return $this->mToken->getToken();
    }

    /**
     * @return AccessToken|null
     */
    public function getAccessToken() : ?AccessToken
    {
        return $this->mToken;
    }

    private function getUrlWithQueryParameters(string $pApiEndpoint, array $pParameters): string
    {

        // Ensure correct encoding of parameters provided
        $lParameterFields =
            implode(
                '&',
                array_map(
                    function (string $pKey, $pValue) {
                        if (is_array($pValue)) {
                            $lValue = implode(
                                ',',
                                array_map(
                                    function (string $pElementValue) {
                                        return urlencode($pElementValue);
                                    },
                                    $pValue));
                        } else {
                            $lValue = urlencode($pValue);
                        }

                        return $pKey . '=' . $lValue;
                    },
                    array_keys($pParameters),
                    array_values($pParameters)));

        $lUrl = $this->cApiUrl . $pApiEndpoint;
        if (count($pParameters) > 0) {
            $lUrl .= '?' . $lParameterFields;
        }

        return $lUrl;
    }

    private function checkAndRenewToken()
    {


        if($this->mToken->hasExpired()) {
            $newToken = $this->mApaleoProvider->getAccessToken('refreshToken', [
                'refresh_token' => $this->mToken->getRefreshToken()
            ]);

            $this->mToken = $newToken;
        }

    }


    /**
     * @brief Perform a GET request to the Apaleo API and return the decoded json data or null on error
     */
    public function get(string $pApiEndpoint, array $pParameters): ?\stdClass
    {



        $lHeaders = ['Accept: application/json', 'Authorization: Bearer ' . $this->getToken()];

        $lUrl = $this->getUrlWithQueryParameters($pApiEndpoint, $pParameters);

        $lChannel = curl_init($lUrl);
        curl_setopt($lChannel, CURLOPT_CONNECTTIMEOUT, 20);
        curl_setopt($lChannel, CURLOPT_TIMEOUT, 60);
        curl_setopt($lChannel, CURLOPT_HEADER, 0);
        curl_setopt($lChannel, CURLOPT_FOLLOWLOCATION, 0);
        curl_setopt($lChannel, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($lChannel, CURLOPT_REFERER, $this->mClientUrl);
        curl_setopt($lChannel, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($lChannel, CURLOPT_HTTPHEADER, $lHeaders);

        $lResponseData = curl_exec($lChannel);
        $lStatusCode = curl_getinfo($lChannel, CURLINFO_HTTP_CODE);

        if ($lResponseData === false || $lStatusCode < 200 || $lStatusCode >= 300) {
            $lInfo = curl_getinfo($lChannel);
            $lErrorMessage = sprintf('curl_exec failed during transfer: (http code: %d, url: %s, error: %s)',
                $lInfo['http_code'],
                $lInfo['url'],
                curl_error($lChannel));

            curl_close($lChannel);

            throw new \RuntimeException($lErrorMessage, $lStatusCode);
        }

        curl_close($lChannel);

        return json_decode($lResponseData);
    }

    /**
     * @brief Perform a PUT request to the Apaleo API and return the decoded json data or null on error
     */
    public function put(string $pApiEndpoint, array $pParameters): ?\stdClass
    {



        $lHeaders = ['Accept: application/json', 'Authorization: Bearer ' . $this->getToken()];

        $lUrl = $this->getUrlWithQueryParameters($pApiEndpoint, $pParameters);

        $lChannel = curl_init($lUrl);
        curl_setopt($lChannel, CURLOPT_PUT, 1);
        curl_setopt($lChannel, CURLOPT_HEADER, 0);
        curl_setopt($lChannel, CURLOPT_FOLLOWLOCATION, 0);
        curl_setopt($lChannel, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($lChannel, CURLOPT_REFERER, $this->mClientUrl);
        curl_setopt($lChannel, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($lChannel, CURLOPT_HTTPHEADER, $lHeaders);
        curl_setopt($lChannel, CURLOPT_CONNECTTIMEOUT, 20);
        curl_setopt($lChannel, CURLOPT_TIMEOUT, 60);

        $lResponseData = curl_exec($lChannel);
        $lStatusCode = curl_getinfo($lChannel, CURLINFO_HTTP_CODE);

        if ($lResponseData === false || $lStatusCode < 200 || $lStatusCode >= 300) {
            $lInfo = curl_getinfo($lChannel);
            $lErrorMessage = sprintf('curl_exec failed during transfer: (http code: %d, url: %s, error: %s)',
                $lInfo['http_code'],
                $lInfo['url'],
                curl_error($lChannel));

            curl_close($lChannel);

            throw new \RuntimeException($lErrorMessage, $lStatusCode);
        }

        curl_close($lChannel);

        return json_decode($lResponseData);
    }

    /**
     * @brief Perform a GET request to the Apaleo API and return the decoded json data or null on error
     */
    public function post(string $pApiEndpoint, array $pParameters): ?\stdClass
    {



        $lHeaders = ['Accept: application/json', 'Authorization: Bearer ' . $this->getToken()];

        $lUrl = $this->getUrlWithQueryParameters($pApiEndpoint, $pParameters);

        $lChannel = curl_init($lUrl);
        curl_setopt($lChannel, CURLOPT_HEADER, 0);
        curl_setopt($lChannel, CURLOPT_POST, 1);
        curl_setopt($lChannel, CURLOPT_POSTFIELDS,false);
        curl_setopt($lChannel, CURLOPT_FOLLOWLOCATION, 0);
        curl_setopt($lChannel, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($lChannel, CURLOPT_REFERER, $this->mClientUrl);
        curl_setopt($lChannel, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($lChannel, CURLOPT_HTTPHEADER, $lHeaders);
        curl_setopt($lChannel, CURLOPT_CONNECTTIMEOUT, 20);
        curl_setopt($lChannel, CURLOPT_TIMEOUT, 60);

        $lResponseData = curl_exec($lChannel);
        $lStatusCode = curl_getinfo($lChannel, CURLINFO_HTTP_CODE);

        if ($lResponseData === false || $lStatusCode < 200 || $lStatusCode >= 300) {
            $lInfo = curl_getinfo($lChannel);
            $lErrorMessage = sprintf('curl_exec failed during transfer: (http code: %d, url: %s, error: %s)',
                $lInfo['http_code'],
                $lInfo['url'],
                curl_error($lChannel));

            curl_close($lChannel);

            throw new \RuntimeException($lErrorMessage, $lStatusCode);
        }

        curl_close($lChannel);

        return json_decode($lResponseData);
    }
}

