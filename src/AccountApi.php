<?php

namespace BYSolutions\Apaleo\Client;

class AccountApi extends ApiBase
{
	public function getCurrent()
    {
		return $this->get('account/v1/accounts/current', []);
    }
}
