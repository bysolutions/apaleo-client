<?php
/**
 * PHP Implementation of Apaleo API.
 *
 * Copyright 2017 Loopon AB. See LICENSE for details (MIT License; free to use as you wish).
 *
 * This code is provided as-is without warranty of any kind. If you do have questions/need assistance with this example,
 * feel free to contact Simon Finne <simon.finne@loopon.com> where I will try to respond if possible, but cannot
 * guarantee any support.
 *
 * Note, that if you for any reason want to connect to Apaleo's staging environment, you need to update the
 * ApaleoProvider::cIdentityUrl and ApaleoClient::cApiUrl configurations.
 *
 * Contact api@apaleo.com or see dev.apaleo.com for help regarding their api.
 */

namespace BYSolutions\Apaleo\Client;

/**
 * @brief Implementation of Apaleo RatePlan API
 */
class FinanceApi extends ApiBase
{


    /**
     * @param string $propertyId
     * @param \DateTime $from
     * @param \DateTime $to
     * @param array $otherParams
     * @return \stdClass|null
     */
    public function postFinanceExport(string $propertyId, \DateTime $from, \DateTime $to, $otherParams = [])
    {

        $params = [
            'propertyId' => $propertyId,
            'from' => $from->format(\DateTime::ATOM),
            'to' => $to->format(\DateTime::ATOM),
        ];

        $params = array_merge($params, $otherParams);

        return $this->post('finance/v1/accounts/export',$params);

    }

    public function getFolios(string $propertyId, $reservationIds, $otherParams = [])
    {
        $params = [
            'propertyId' => $propertyId,

        ];
        if($reservationIds) {
            $params['reservationIds'] = $reservationIds;
        }


        $params = array_merge($params, $otherParams);
        return $this->get('finance/v1/folios', $params);

    }

    public function getFolio($folioId)
    {
        return $this->get('finance/v1/folios/'.$folioId, []);
    }

    public function getInvoice($invoiceId)
    {
        return $this->get('finance/v1/invoices/'.$invoiceId, []);
    }

    public function getInvoices($reservationId)
    {
        $params['reservationId'] = $reservationId;
        return $this->get('finance/v1/invoices', $params);
    }

    public function getVatTypes($countryCode)
    {
        $vatTypes = $this->get('finance/v1/types/vat', ['isoCountryCode' => $countryCode]);
        if (!isset($vatTypes->vatTypes)) {
            return [];
        }

        return $vatTypes->vatTypes;
    }
    public function getServiceTypes()
    {
        $serviceTypes = $this->get('finance/v1/types/service-types', []);
        if (!isset($serviceTypes->serviceTypes)) {
            return [];
        }

        return $serviceTypes->serviceTypes;
    }




}
